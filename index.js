// Use the "require" directive to load Node.js
// HTTP is a protocol that allows the fetching of resources such as HTML documents
const http = require('http');


const port = 4000;

http.createServer((request, response) => {

	if (request.url == "/items" && request.method == "GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data Retrieved from the database');
	}

	else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not Found!');
	}

}).listen(port);
// A port is a virtual point where network connections start & end
// The server will be assigned to port 4000 via the ".listen(4000)"


// When server is running, console will print the message
console.log(`Server Running at localhost: ${port}.`);
